import { Injectable, Component, EventEmitter, Output } from '@angular/core';
import { Day } from './calendar/calendar.state';
import { HttpClient } from '@angular/common/http';

export abstract class IChronicleService {
  abstract get(): void;
  abstract toggle(day : Day) : void;
  abstract isOpen : boolean;
  abstract day : Day;
  abstract change : EventEmitter<boolean>;
  abstract httpClient : HttpClient;

  constructor(httpClient: HttpClient) {
  }
}
