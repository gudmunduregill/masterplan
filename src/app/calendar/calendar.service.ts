import { Injectable, HostListener } from '@angular/core';
import * as $ from 'jquery';
import * as moment from 'moment';
import { CalendarHelper } from './helpers/helpers';
import { Moment } from 'moment';
import { CalendarState } from './calendar.state';
import { Day } from './calendar.state';
import { IChronicleService } from '../chronicle.service.interface';

@Injectable({
  providedIn: 'root'
})

// Returns a pair [W_split, H_split] with values for how many times
// we should divide width and height respectively to get a grid (calendar)
// of squares.

export class CalendarService {

  settings = {
    shouldBeScrollable: true
  }

  _helper : CalendarHelper;

  _currentDay : Moment;
  _currentDayText : string;
  _textMeasurement : {width};
  _textWidth : number;
  _textHeight: number;
  _fontAsPercentageOfWidth : number;
  _fontTopMarginAsPercentageOfHeight : number;
  _fontSize : number;
  _ctx : CanvasRenderingContext2D;
  _widthOfSplit : number;
  _heightOfSplit : number;
  _body : JQuery<HTMLElement>;

  _calendarState : CalendarState;

  CANVAS_HEIGHT: number;
  CANVAS_WIDTH: any;
  MARGIN: number;

  drawDateTextForDay(day : Day) : void {
    this._ctx.fillText(day.date.format("D.M.YYYY"), day.x + 2, day.y + this._fontSize);
  }

  drawDates() : void {
    this._fontSize = Math.round(this._fontAsPercentageOfWidth * this._widthOfSplit);
    if (this._fontSize > 4) {
      this._ctx.font = this._fontSize + "px Arial";
      this._calendarState.days.forEach( (day) => {
        this.drawDateTextForDay(day);
      });
    }
  }

  drawBackground() : void {
      // --> Drawing our background canvas itself
      this._ctx.canvas.width = this._calendarState.getCanvasWidth();
      this._ctx.canvas.height = this._calendarState.getCanvasHeight();
      this._ctx.fillStyle = "#363D49";
      this._ctx.fillRect(0, 0, this._calendarState.getCanvasWidth(), this._calendarState.getCanvasHeight());
      this._ctx.fillStyle = "white";
      // <--  Drawing our background canvas itself

      // --> Draw box around calendar
      let x1, x2, y1, y2;
      x1 = this.MARGIN;
      x2 = this._calendarState.getCalendarWidth();
      y1 = this.MARGIN;
      y2 = this._calendarState.getCalendarHeight();
      this._ctx.rect(x1, y1, x2, y2);
      this._ctx.strokeStyle = '#FFFFFF';
      // <-- Draw box around calendar
  }

  //addVerticalLinesToCtx(splitValues) {
      //// Draw lines down
      //this._widthOfSplit = this._calendarState.getCalendarWidth() / splitValues.widthSplit;
      //let placeOfWidthSplit = this.MARGIN + this._widthOfSplit;
      //for (let i = 0; i < splitValues.widthSplit; i++) {
        //this._ctx.moveTo(placeOfWidthSplit, this.MARGIN);
        //this._ctx.lineTo(placeOfWidthSplit, this._calendarState.getCalendarHeight() + this.MARGIN);
        //let column = {x1: placeOfWidthSplit, y1: this.MARGIN, x2: this._widthOfSplit, y2: this._calendarState.getCalendarHeight()};
        //placeOfWidthSplit += this._widthOfSplit;
      //}
  //}

  //addHorizontalLinesToCtx(splitValues) {
  //    // Draw lines from left to right
  //    this._heightOfSplit = this._calendarState.getCalendarHeight() / splitValues.heightSplit;
  //    let placeOfHeightSplit = this.MARGIN + this._heightOfSplit;
  //    for (let i = 0; i < splitValues.heightSplit; i++) {
  //      this._ctx.moveTo(this.MARGIN, placeOfHeightSplit);
  //      this._ctx.lineTo(this._calendarState.getCalendarWidth() + this.MARGIN, placeOfHeightSplit);
  //      placeOfHeightSplit += this._heightOfSplit;
  //    }
//
  //}

  //drawLines(splitValues) : void {
  //  this.addVerticalLinesToCtx(splitValues);
  //  this.addHorizontalLinesToCtx(splitValues);
  //  this._ctx.stroke();
  //}

  getClickXY(event, margin) {
    let canvasXoffset = ($('canvas')[0]).getBoundingClientRect().left; // Offset of canvas on client browser, to help find client click on canvas
    let canvasYoffset = ($('canvas')[0]).getBoundingClientRect().top;
    let clientClickX = event.clientX - canvasXoffset - margin;
    let clientClickY = event.clientY - canvasYoffset - margin;
    return {x: clientClickX, y: clientClickY};
  }

  drawClickedDay(clickedDay) : void {
    let width = this._calendarState.dayRectangleWidth;
    let height = this._calendarState.dayRectangleHeight;
    this._ctx.fillRect(clickedDay.x, clickedDay.y, width, height);
    this._ctx.fillStyle = "#363D49";
    this.drawDateTextForDay(clickedDay);
  }

  drawCalendar() {
    return new Promise( resolve => {
      $(document).ready( () => {
        this._ctx.clearRect(0, 0, this._ctx.canvas.width, this._ctx.canvas.height);
        this.drawBackground();
        let splitValues = this._calendarState.getSplitValues();
        //this.drawLines(splitValues);
        //this.drawDates();
        this.drawDays();
        resolve();
      })
    })
  }

  drawDays() {
    console.log(this._calendarState.days);
    this._calendarState.days.forEach( (day) => {

      console.log(day);
      let x1 = day.x;
      let y1 = day.y;
      let x2 = x1 + this._calendarState.dayRectangleWidth - this._calendarState.MARGIN;
      let y2 = y1 + this._calendarState.dayRectangleHeight - this._calendarState.MARGIN;
      this._ctx.rect(x1, y1, x2, y2);
      this._ctx.strokeStyle = '#FFFFFF';
    });
    this._ctx.stroke();
  }

  getClickedDay() {
    let clientClick = this.getClickXY(event, this.MARGIN);
    let width = this._calendarState.dayRectangleWidth;
    let height = this._calendarState.dayRectangleHeight;
    let clickedDay = this._calendarState.days.filter( (day) => {
      return (day.x < clientClick.x
              && day.y < clientClick.y
              && (day.x + width) > clientClick.x
              && (day.y + height) > clientClick.y);
    })[0];
    return clickedDay;
  }

  constructor(private chronicleService : IChronicleService ) {
    $(document).ready( () => {
      $('#calendar_canvas').click( (event) => {
        let clickedDay = this.getClickedDay();
        this._calendarState.setSelectedDay(clickedDay);
        this.chronicleService.toggle(clickedDay);
        this._calendarState.setCanvasWidth(this._body.innerWidth() - ($('app-chronicle')[0]).offsetWidth);
        this.drawCalendar().then( resolve => {
          this.drawClickedDay(this._calendarState.getSelectedDay());
        });
      });

      this._body = $('body');
      this.CANVAS_HEIGHT = this._body.innerHeight() - $('nav').outerHeight() - 10;
      this.CANVAS_WIDTH = this._body.innerWidth();
      this._calendarState = new CalendarState(this.CANVAS_WIDTH, this.CANVAS_HEIGHT);
      let canvas = <HTMLCanvasElement> document.getElementById('calendar_canvas');
      this._ctx = canvas.getContext('2d');
      this._currentDay = this._calendarState.getStartDate();
      this._currentDayText = this._currentDay.format("D.M.YYYY");
      this._textMeasurement = this._ctx.measureText(this._currentDayText);
      this._textWidth = this._textMeasurement.width;
      this._textHeight = this._helper.getTextHeight(this._currentDayText, this._ctx.font).height;
      this._ctx.font = "150% Arial";
      this._fontAsPercentageOfWidth = 0.16;
      this._fontTopMarginAsPercentageOfHeight = 0.15;
      this._fontSize = 15;
      this.MARGIN = this._calendarState.MARGIN;
    })

    this._helper = new CalendarHelper();
    // Zoom in and out of calendar on mousescroll
    window.addEventListener('wheel', (e)  => {
      if (this.settings.shouldBeScrollable) {
        if (e.deltaY < 0 && this.settings.shouldBeScrollable) {
          if (this._calendarState.getNumberOfDaysDisplayed() > 3) {
            let newNumberOfDaysDisplayed = this._calendarState.getNumberOfDaysDisplayed() / 2.7;
            this._calendarState.setNumberOfDaysDisplayed(newNumberOfDaysDisplayed);
            this.drawCalendar();
          }
        } else {
          if (this._calendarState.getNumberOfDaysDisplayed() < 10000 && this.settings.shouldBeScrollable) {
            this.settings.shouldBeScrollable = false;
            let newNumberOfDaysDisplayed = this._calendarState.getNumberOfDaysDisplayed() * 2.7;
            this._calendarState.setNumberOfDaysDisplayed(newNumberOfDaysDisplayed);
            this.drawCalendar();
          }
        }
        this.settings.shouldBeScrollable = true;
      }
    });
  }
}
