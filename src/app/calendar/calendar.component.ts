import { Component, OnInit, HostListener } from '@angular/core';
import { CalendarService } from './calendar.service';
import { IChronicleService } from '../chronicle.service.interface';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})

export class CalendarComponent implements OnInit {

  days: number[];
  _calendarService : CalendarService;

  ngOnInit(): void {}

  numberOfDaysToDisplay: number;
  startDay: Date;

  constructor(private chronicleService : IChronicleService) {
    this.numberOfDaysToDisplay = 1;
    this.startDay = new Date;
    this.days = [1,2,3];
    this._calendarService = new CalendarService(this.chronicleService);
    this._calendarService.drawCalendar();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this._calendarService.drawCalendar();
    event.target.innerWidth;
  }


}
