export class CalendarHelper {

    getTextHeight(targetText, font) : {ascent, height} {

      var text = $('<p>' + targetText + '<p>').css({ fontFamily: font });
      var block = $('<div style="display: inline-block; width: 1px; height: 0px;"></div>');

      var div = $('<div></div>');
      div.append(text, block);

      var body = $('body');
      body.append(div);

      let result = null;

      try {

        result = {};

        block.css({ verticalAlign: 'baseline' });
        result.ascent = block.offset().top - text.offset().top;

        block.css({ verticalAlign: 'bottom' });
        result.height = block.offset().top - text.offset().top;

        result.descent = result.height - result.ascent;

        return result;
      } finally {
        div.remove();
      }
    };

}
