import * as moment from 'moment';
import { Moment } from 'moment';

/* The Purpose of this class is to provide an object which fully and accurately
the state of everything calendarwise which is to be drawn on the client's canvas.
This allows us to go through only one object whenever something is changed and
needs to be redrawn, making sure that at every change the object is redrawn.
TODO: make this object is represented fully in database and that it fully represents
the relevant data in the database.
1. Represent the state which is to be drawn on canvas
2. Represents the data in database (CRUD on changes)
*/

export class Day {
  public date : Moment;
  public x : number;
  public y : number;
  constructor(date : Moment, x : number, y : number) {
      this.date = date;
      this.x = x;
      this.y = y;
  }
}

export class CalendarState {

  public MARGIN : number;
  public days : Array<Day>;
  public dayRectangleWidth : number;
  public dayRectangleHeight : number;
  private readonly _startDate : Moment = moment();


  _selectedDay : Day
  _canvasWidth : number;
  _canvasHeight : number;

  _calendarWidth : number;
  _calendarHeight : number;

  _numberOfDaysOnXaxis : number;
  _numberOfDaysOnYaxis : number;
  _numberOfDaysDisplayed : number;

  public getSelectedDay() {
    return this._selectedDay;
  }

  public setSelectedDay(selectedDay : Day) {
    this._selectedDay = selectedDay;
  }

  public getCalendarHeight() : number {
      return this._calendarHeight;
  }

  public getCalendarWidth() : number {
      return this._calendarWidth;
  }

  public setCalendarWidth(n : number) : void {
    this._calendarWidth = n;
  }

  public setCalendarHeight(n : number) : void {
    this._calendarHeight = n;
  }

  public getCanvasWidth() : number {
      return this._canvasWidth;
  }

  public setCanvasWidth(n : number) : void {
    this._canvasWidth = n;
    this.updateStateVariables(this._canvasWidth, this._canvasHeight);
    this.days = this.generateDays();
  }

  public getCanvasHeight() : number {
      return this._canvasHeight;
  }

  public setCanvasHeight(n : number) : void {
    this._canvasHeight = n;
  }

  constructor(canvasWidth, canvasHeight) {
    this.MARGIN = 5;
    this._numberOfDaysDisplayed = 30;
    this._canvasWidth = canvasWidth;
    this._canvasHeight = canvasHeight;
    this.updateStateVariables(this._canvasWidth, this._canvasHeight);
  }

  public getStartDate() : Moment {
    return moment();
  }

  getValuesToSplitIntoGrid(numberOfDays, width, height) : {widthSplit : number, heightSplit : number}  {
    let divisor = Math.round(Math.sqrt(width * height / numberOfDays));
    return {widthSplit: Math.round(width/divisor), heightSplit: Math.round(height/divisor)}
  }

  public getSplitValues() {
    let numberOfDaysToDisplay = this.getNumberOfDaysDisplayed();
    let width = this._calendarWidth - this.MARGIN;
    let height = this._calendarHeight - this.MARGIN;
    return this.getValuesToSplitIntoGrid(numberOfDaysToDisplay, width, height);
  }

  updateStateVariables(canvasWidth, canvasHeight) : void {
    this._calendarWidth = canvasWidth - (this.MARGIN );
    this._calendarHeight = canvasHeight - (this.MARGIN );
    let dayDivisor = Math.round(Math.sqrt(canvasWidth * canvasHeight / this._numberOfDaysDisplayed));
    this._numberOfDaysOnXaxis = Math.round(canvasWidth / dayDivisor);
    this._numberOfDaysOnYaxis = Math.round(canvasHeight / dayDivisor);
    this.dayRectangleWidth = (canvasWidth - this.MARGIN) / this._numberOfDaysOnXaxis;
    this.dayRectangleHeight = (canvasHeight - this.MARGIN) / this._numberOfDaysOnYaxis;
    this.days = this.generateDays();
  }

  public getNumberOfDaysDisplayed() {
    return this._numberOfDaysDisplayed;
  }

  public setNumberOfDaysDisplayed(newNumberOfDaysDisplayed) {
    this._numberOfDaysDisplayed = newNumberOfDaysDisplayed;
    this.updateStateVariables(this._canvasWidth, this._canvasHeight);
    this.days = this.generateDays();
  }

  generateDays() : Array<Day>{
    let days = new Array<Day>();
    let y = this.MARGIN;
    let date = this._startDate.clone();

    for (let i = 0; i < this._numberOfDaysOnYaxis; i++) {
      let x = this.MARGIN;
      for (let j = 0; j < this._numberOfDaysOnXaxis; j++) {
        date = date.add(1, 'days');
        let dateText = date.format('D.M.YYYY');

        let day = new Day(date.clone(), x, y);

        if (this._selectedDay != undefined && this._selectedDay.date.format('D.M.YYYY') === dateText) {
          // transfer selected day object to this clone, so it will updateStateVariablesalong with the rest of the state when it changes
          this._selectedDay = day;
        }
        days.push(day);
        x += this.dayRectangleWidth;
      }
      y += this.dayRectangleHeight;
    }
    return days;
  }
}
