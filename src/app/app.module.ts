import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CalendarComponent } from './calendar/calendar.component';
import { CalendarToolboxComponent } from './calendar-toolbox/calendar-toolbox.component';
import { ChronicleComponent } from './chronicle/chronicle.component';
import { ChronicleService } from './chronicle/chronicle.service';
import { CalendarService } from './calendar/calendar.service';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { IChronicleService } from './chronicle.service.interface';

//function MainServiceFactory() {
  //return new ChronicleService();
//}

@NgModule({
  declarations: [
    AppComponent,
    CalendarComponent,
    CalendarToolboxComponent,
    ChronicleComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [{
    provide: IChronicleService,
    useClass: ChronicleService
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
