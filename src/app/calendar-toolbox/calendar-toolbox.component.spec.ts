import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarToolboxComponent } from './calendar-toolbox.component';

describe('CalendarToolboxComponent', () => {
  let component: CalendarToolboxComponent;
  let fixture: ComponentFixture<CalendarToolboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendarToolboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarToolboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
