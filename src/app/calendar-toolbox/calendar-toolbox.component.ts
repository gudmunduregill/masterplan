import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calendar-toolbox',
  templateUrl: './calendar-toolbox.component.html',
  styleUrls: ['./calendar-toolbox.component.scss']
})
export class CalendarToolboxComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  navbarOpen = false;

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }
  
}
