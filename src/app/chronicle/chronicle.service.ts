declare var require : any;
import { Injectable, Component, EventEmitter, Output } from '@angular/core';
import { Day } from '../calendar/calendar.state';
import { IChronicleService } from '../chronicle.service.interface';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ChronicleService implements IChronicleService {
  isOpen = false;
  day : Day;
  httpClient : HttpClient;

  @Output() change : EventEmitter<boolean> = new EventEmitter();
  constructor(httpClient: HttpClient) {
  }

  toggle(day : Day) : void {
    this.day = day;
    //this.isOpen = !this.isOpen;
    this.isOpen = true;
    this.change.emit(this.isOpen);
    }

    get(): void {
      this.httpClient.get('url');
    }

}
