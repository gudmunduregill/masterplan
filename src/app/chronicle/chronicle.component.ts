import { Component, HostBinding, NgZone } from '@angular/core';
import { IChronicleService } from '../chronicle.service.interface';
import { Moment } from 'moment';
import * as moment from 'moment';
import { Day } from '../calendar/calendar.state';

@Component({
  selector: 'app-chronicle',
  templateUrl: './chronicle.component.html',
  styleUrls: ['./chronicle.component.scss']
})

export class ChronicleComponent {

  _record : string;
  _chronicle : Chronicle;

  @HostBinding('class.is-open')
  isOpen = false;

  constructor(
    private chronicleService : IChronicleService,
    private ngZone: NgZone
  ) {
    this._chronicle = new Chronicle(new Day(moment(), 0, 0))
  }

  recordChanged(changes) {
    console.log(changes);
  }

  ngOnInit() {
    this.chronicleService.change.subscribe(isOpen => {
      this.ngZone.run( () => {
        this.isOpen = isOpen;
        this._chronicle.day = this.chronicleService.day;
      });
    });
  }

}

class Chronicle {
  public day : Day
  public record : string
  constructor(day : Day) {
    this.day = day;
  }
}
