var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import * as $ from 'jquery';
var CalendarService = /** @class */ (function () {
    function CalendarService() {
        //this.settings.splitCalendarOnLonger = true;
    }
    CalendarService.prototype.drawCalendar = function () {
        $(document).ready(function () {
            var body = $('body');
            var lineChartElement = document.getElementById('calendar_canvas');
            var ctx = lineChartElement.getContext('2d');
            var HEIGHT = body.innerHeight();
            var WIDTH = body.innerWidth();
            var MARGIN = 5;
            ctx.canvas.width = WIDTH;
            ctx.canvas.height = HEIGHT;
            ctx.fillStyle = "#363D49";
            ctx.fillRect(0, 0, WIDTH, HEIGHT);
            ctx.fillStyle = "white";
            // <-- Draw box around calendar
            var x1, x2, y1, y2;
            x1 = MARGIN;
            x2 = WIDTH - MARGIN * 2;
            y1 = MARGIN;
            y2 = HEIGHT - MARGIN * 2;
            ctx.rect(x1, y1, x2, y2);
            ctx.strokeStyle = '#FFFFFF';
            // --> Draw box around calendar
            // <-- Draw n days on calendar
            // 2, 4, 6, 8
            for (var step = 0; step < 1; ++step) {
                step *= 2;
                ctx.moveTo(MARGIN, MARGIN);
                ctx.lineTo(150, 150);
                ctx.stroke();
            }
        });
    };
    CalendarService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], CalendarService);
    return CalendarService;
}());
export { CalendarService };
//# sourceMappingURL=calendar.service.js.map