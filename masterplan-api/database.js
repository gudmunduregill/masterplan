// database.js
// ========
exports.MongoClient = require('mongodb').MongoClient;
const test = require('assert');
// Connection url
const url = 'mongodb://masterplanner:master0fplans@ds211592.mlab.com:11592/masterplan';
// Database Name
const dbName = 'masterplan';
// Connect using MongoClient

// set up a command function
var getDbStats = function(db, callback) {
      db.command({'dbStats': 1},
      function(err, results) {
        console.log(results);
        callback();
    }
  );
};
