'use strict';

var utils = require('../utils/writer.js');
var Unauthenticated = require('../service/UnauthenticatedService');

module.exports.addUser = function addUser (req, res, next) {
  var userObject = req.swagger.params['userObject'].value;
  Unauthenticated.addUser(userObject)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
