'use strict';

var utils = require('../utils/writer.js');
var User = require('../service/UserService');

module.exports.addDay = function addDay (req, res, next) {
  var dayObject = req.swagger.params['dayObject'].value;
  User.addDay(dayObject)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getAllDays = function getDayAllDays (req, res, next) {
  //var userId = req.swagger.params['searchString'].value;
  //var skip = req.swagger.params['skip'].value;
  //var limit = req.swagger.params['limit'].value;
  User.getDay()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getDay = function getDay (req, res, next) {
  let dateString = req.swagger.params['dateString'].value;
  User.getDay(dateString)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
