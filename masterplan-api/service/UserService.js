'use strict';
const database = require('../database'); // MongoClient
const url = 'mongodb://masterplanner:master0fplans@ds211592.mlab.com:11592/masterplan';

/**
 * adds a day object
 * Adds a day to the system
 *
 * dayObject Day Day object to add (optional)
 * no response value expected for this operation
 **/
exports.addDay = function(dayObject) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}



/**
 * retrieves day
 * By passing in the appropriate options, you can search for available days in the system
 *
 * searchString String pass an optional search string for looking up inventory (optional)
 * skip Integer number of records to skip for pagination (optional)
 * limit Integer maximum number of records to return (optional)
 * returns List
 **/
exports.getDay = function(dateString) {
  return new Promise(function(resolve, reject) {
    let result = {};
    database.MongoClient.connect(url, function(err, client) {
      let daysCollection = client.db('masterplan').collection('days');
      daysCollection.find({date: dateString}).toArray(function(err, docs) {
        console.log(docs);
        client.close();
        resolve(docs[0]);
      });
    })
  });
}

exports.getAllDays = function() {
  return new Promise(function(resolve, reject) {
    let result = {};
    database.MongoClient.connect(url, function(err, client) {
      let daysCollection = client.db('masterplan').collection('days');
      daysCollection.find({}).toArray(function(err, docs) {
        console.log(docs);
        client.close();
        resolve(docs);
      });
    })
  });
}

//    var examples = {};
//    examples['application/json'] = [ {
//  "date" : "18.12.1988",
//  "day_notes" : "This was an interesting day. I didn't meet all my goals but it definitely showed progress. I was relying to much on everything being ready when I started, but that was a false assumption.",
//  "id" : "d290f1ee-6c54-4b01-90e6-d701748f0851"
//}, {
//  "date" : "18.12.1988",
//  "day_notes" : "This was an interesting day. I didn't meet all my goals but it definitely showed progress. I was relying to much on everything being ready when I started, but that was a false assumption.",
//  "id" : "d290f1ee-6c54-4b01-90e6-d701748f0851"
//} ];
//    if (Object.keys(examples).length > 0) {
//      console.log(database.MongoClient);
//      resolve(examples[Object.keys(examples)[0]]);
//    } else {
//    }
